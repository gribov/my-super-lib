const _ = require('lodash');

const MySuperLib = {
  getKeysString: function (target, key, glue) {
    return _.map(target, key).join(glue || '_');
  },
}

module.exports = MySuperLib;